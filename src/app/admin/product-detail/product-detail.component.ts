import { Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  loading:any;

  constructor(
    public dialogRef: MatDialogRef<ProductDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public api: ApiService

  ) { }

  ngOnInit(): void {
  }

  saveData() {
    //jika id tidak terdefinisi maka buat data
    if (this.data.id == undefined) {
      //prosedur pengiriman data ke server menggunakan metode POST
      this.api.post('books', this.data).subscribe(result => {
        //tutup dialog dan kirimkan feedback server ke fungsi pemanggil dialog
        this.dialogRef.close(result);
      }, error => {
        //kondisi jika terjadi masalah pengiriman pada pengiriman data
        alert(error);
      })
    } else {
      //prosedur edit data menggunakan metode PUT
      this.api.post('books/'+this.data.id,this.data).subscribe(result=>{
        //tutup dialog dan kirimkan feedback server ke fungsi pemanggil dialog
        this.dialogRef.close(result);
      },error=>{
        alert(error);
      })
    }

  }


}
