import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';
import { HomeComponent } from './public/home/home.component';
import { ForgotComponent } from './auth/forgot/forgot.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path:'forgot', component:ForgotComponent },
  { path: 'admin', loadChildren: ()=>import('./admin/admin.module').then(mod=>mod.AdminModule)},
  { path: '', pathMatch: 'full', redirectTo:'/login'},
  { path: 'public', loadChildren: ()=>import('./public/public.module').then(mod=>mod.PublicModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
