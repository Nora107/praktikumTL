import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  //inisial untuk data formuulir
  user: any = {};
  hide:boolean=true;
  constructor(
    public api: ApiService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  //form validation
  // email = new FormControl('', [Validators.required, Validators.email]);
  // password = new FormControl('', [Validators.required]);

  //register
  loading: any;
  login(user: any) {
    this.loading = true;
    this.api.login(user.email, user.password).subscribe((res: any) => {
      this.loading = false;
      localStorage.setItem('appToken', JSON.stringify(res));
      this.router.navigate(['admin/dashboard']);
    }, (error: any) => {
      this.loading = false;
      alert('Tidak dapat login');
    });
  }

}
