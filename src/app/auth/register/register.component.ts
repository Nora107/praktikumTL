import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(
    public api:ApiService,
    public router:Router,
    private db:AngularFirestore,
    public afAuth: AngularFireAuth,
  ) { }

  ngOnInit(): void {
  }

  //form validation
  // email = new FormControl ('', Validators.required, [Validators.email]);
  // password = new FormControl('',[Validators.minLength(6),Validators.required]);
    //inisial untuk data formulir
  loading:any;
  user:any={};
  hide:boolean=true;

  register(){
    this.loading=true;
    this.api.register(this.user.email, this.user.password).subscribe(res=>{
      console.log(res);
      this.loading=false;
      this.router.navigate(['auth/login']);
    }, error=>{
      this.loading=false;
      alert('Tidak dapat mendaftar')
    })
  }


// email:any={};
// password:any={};
//   register() {
//     this.loading=true;
//     this.afAuth.createUserWithEmailAndPassword(this.email, this.password)
//     .then((res: any)=>{
//       localStorage.setItem('user',JSON.stringify(res))
//       }).catch((error: any)=>{
//       this.loading=false;
//       alert('Tidak dapat mendaftar')
//     })

//   }

  
   


}

