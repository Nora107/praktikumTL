// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
  apiKey: "AIzaSyAC3JlrKQ-Zo2YNhscaJ40g2veOWVZU1II",
  authDomain: "stang-d71af.firebaseapp.com",
  databaseURL: "https://stang-d71af-default-rtdb.firebaseio.com",
  projectId: "stang-d71af",
  storageBucket: "stang-d71af.appspot.com",
  messagingSenderId: "595832025091",
  appId: "1:595832025091:web:6077e16f98997919a75e36",
  measurementId: "G-8NF0T1SXLQ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
